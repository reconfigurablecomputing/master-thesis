# Report Master Thesis

Report master thesis


When using vscode, add -shell-escape line to the settings file

    "latex-workshop.latex.tools": [

      {
        "name": "latexmk",
        "command": "latexmk",
        "args": [
          "-shell-escape",              // <---- added this line
          "-synctex=1",
          "-interaction=nonstopmode",
          "-file-line-error",
          "-pdf",
          "-outdir=%OUTDIR%",
          "%DOC%"
        ],
        "env": {}
      },

Install pygments for minted
sudo apt-get install python3-pygments

https://michielkempen.com/blog/beautiful-code-snippets-in-latex-using-the-minted-package-and-visual-studio-code

Add the --shell-escape flag
The configuration option in question was hidden under Latex-workshop > Latex: Tools.
You can quickly find this setting by searching for "@ext:james-yu.latex-workshop tools" in your settings page

Note: if the document is already build once and you get weird errors, try to delete all output build files that latexmk generates.
