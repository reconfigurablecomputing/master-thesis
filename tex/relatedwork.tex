\chapter{Related Work}
\label{chap:relatedwork}
This chapter gives an overview of the \gls{DPR} tools of the leading \gls{FPGA} vendors and the related academic tools found in the literature.
A number of those tools have been selected for comparison on their features and (active) development status.

\section{An Overview of DPR Tools}
\label{sec:overview-dpr-tools}
The major \gls{FPGA} vendors do have support for \gls{DPR}.
Intel Altera supports this for their Cyclone, Arria, and Stratix devices with the Quartus Prime tool \cite{Intel-UG-20136}.
The \gls{PR} design flow of Intel requires initial planning where the design is set up with one or more partitions and the placement in the floorplan.
In the floorplan view, you define the static region, the \gls{PR} place regions and routable regions for interfacing.
The interface planner is used to create periphery floorplan assignments in the design. %p14
The next step is adding the \gls{PR} controller to the project. The \emph{personas} (how Intel names reconfigurable modules) are to be defined next.
After that, the base revision for the design, as well as \gls{PR} implementation revisions for each persona is created.
The Intel \gls{PR} flow works with project revisions to organize several versions in a single project.
At this stage, the base revisions can be compiled together with an export of the static region.
The last step is to generate the \gls{PR} bitstream files and program the \gls{FPGA}.
\par
For Xilinx, designers can use PlanAhead for the ISE Design Suite or their latest software the Vivado Design Suite.
In the Vivado IDE, the partial reconfiguration design flow is to be used for the Virtex, Zynq and UltraScale devices.
Projects using \gls{PR} have to be created with the option partial reconfiguration enabled \cite{UG909,UG947}.
The designer then has to define the number of partitions in the project.
To add and manage the \gls{RM} and the \gls{RTL} sources, the Partial Reconfiguration Wizard is used.
At this point, the project can be synthesized.
Each \gls{RM} is assigned to a \gls{Pblock} by default.
Floorplanning can be carried out to adjust and move the \glspl{Pblock} in the device view.
After passing the \gls{PR}-specific checks, the implementation can be run to place and route all \gls{RM} configurations and the static design.
When the implementation has finished, running \gls{PR} Verify is recommended to ensure consistency between static and reconfigurable part.
The last step is to generate the (partial) bitstream files.
\par
Both tool flows are similar and comparable to each other.
However, the vendor tools do not come without limitations.
For example, the partial region can only host a single module at a time.
They do not have support for slot- or grid-style reconfigurable styles.
This island-only style can lead to a non-optimal use of fabric area.
Furthermore, the vendor tools work with a dependent design flow for building reconfigurable systems.
The advantage of having a single project for the complete reconfigurable system is that the configuration can be checked for integrity.
A disadvantage is that, for example, when the static design requires a change, all of the reconfigurable modules need to be re-implemented, up to the generating of new partial bitstreams.
For large designs, this is undesirable.
This is not the case for an independent design flow.
Here the static system and modules are created independently from each other.
Another limitation is having no support for module relocation.
When the same module is used (or loaded) at various locations on the fabric it is called module relocation.
Module relocation reduces the storage space requirements of the reconfigurable modules and also reduces the number of variants required for a specific module.
\par
To overcome some of these limitations, there exist several academic tools.
As we will see later, design verification can be a challenge for such tools.
The following sections are devoted to the academic tools for the Xilinx platform.

\subsection{GoAhead}
\label{sec:goahead}
\input{tex/goahead.tex}

\subsection{IMPRESS}
\label{sec:IMPRESS}
\input{tex/impress.tex}

\subsection{TedTCL}
\label{sec:TedTCL}
\input{tex/tedtcl.tex}

\section{Comparison and Differences between Frameworks}
\label{sec:related-work-comparision}
Starting with Xilinx Vivado, they have built-in support for \gls{PR}, but island-style only.
Frameworks such as \cite{maverick} completely automate the generation of the partial bitstreams by compiling and configuring \glspl{RM} onto the \gls{PR} region, without the use of vendor tools.
Dreams \cite{DREAMS} has the disadvantage of that modules cannot communicate if they are located nonadjacent.
If modules are located nonadjacent, we can configure the slots in between modules such that they connect these modules.
Those modules are called bypass modules.
\par
Various academic tools were compared on relevant properties on the design flow of implementation \gls{PR} systems.
In particular, the GoAhead tool and IMPRESS framework. They offer more or less the same functionality and there is still active development on them.
For each property in \Cref{tab:comparison-of-dpr-tools}:
\begin{itemize}
  \setlength\itemsep{-0.5em}
  \item Module relocation, the possibility to re-use modules in different partial regions.
  \item Style, the reconfiguration style, e.g. island or any of the other styles.
  \item Partition Interface, this is how the communication is defined between static system and partial modules.
  \item Routing Isolation, how the routing is constrained not to use certain regions.
  \item Automated flow, is the \gls{PR} flow automated, or does it require additional user steps.
  \item Design reconstruction, can the design be reconstructed to a full design.
  \item System architecture description, Does the tool work with a system configuration description.
\end{itemize}

% --------------------------------------------------------------------------------------------------------------
\addtocounter{footnote}{-1}
% table a
\input{tex/comparision-of-dpr-tools.tex}
\footnotetext[1]{Independent or automated flow is not mentioned in the literature.}
\addtocounter{footnote}{1}
\footnotetext[2]{Not fully automated, manual and individual steps have to be performed.}
\addtocounter{footnote}{1}
\footnotetext[3]{This framework only supports design reconstruction for island-style PR.}
% --------------------------------------------------------------------------------------------------------------

\subsection{GoAhead and IMPRESS}
GoAhead has a built-in model of the \gls{FPGA} fabric interconnections (or a graphical device view), TedTCL and IMPRESS do not offer such functionality.
The model has the advantage that tile selections can be done graphically and the internal \gls{PIP} connections can be shown.
A downside is that the chip family database (and possibly the GUI) has to be maintained separately.
Another major difference is that the generated \gls{TCL} constraints scripts from GoAhead must be tied into the build process of the reconfigurable system.
They must be executed at the right time during synthesis and implementation flow within Vivado.
\par
Since GoAhead exists for a long while, it has support for more (older) devices.
Over time, it is extended to support more devices and additional features that have been implemented.
The custom scripting interface gives access to all commands and macros and gives the ability to replay the commands later.
To make use of all commands, it is required to have an in-depth technical knowledge of GoAhead and the \gls{FPGA} chip to be used.
Furthermore, since the tool flow for modules and static design is decoupled, the designer must keep track of the consistency and integrity between modules and static design.
Especially for the correct placement of the interface.
For example, changing the partition size requires the designer to adjust the location (coordinates) of the interface location and tunnels as well as the size of the blocker macro.
Changing a setting or parameter can result in changes required elsewhere, something the designer needs to keep in mind.
\par
Regarding floorplanning, it can be considered more effortless for the IMPRESS framework since it automates most steps.
The designer does not have to know the details of the (GoAhead) commands or have an understanding of the \gls{FPGA} structure.
The project files from IMPRESS are of a similar approach as floorplanning phase of GoAhead.
Main difference is that GoAhead generates \gls{TCL} scripts based on predefined \emph{.goa} configuration files for the static system and for each module.
These must be executed at the right time during the build of the project.
Whereas IMPRESS takes care of this by executing all the necessary commands directly in Vivado when reading the project system architecture files.
\par
Currently, the build process builds all modules and the static system.
Rebuilding a single module is not possible with IMPRESS framework
Whereas GoAhead lets you build a single module by running the template script for each module and perform an implementation run.
\par
GoAhead supports a number of devices, but not all features might have been implemented for all devices.
IMPRESS only supports the Zynq series \glspl{FPGA}, but the tool could be extended to support more devices.
\par
Regarding interface architecture, the IMPRESS framework uses fixed nodes in the device which are shared between static and reconfigurable regions.
This virtual interface has the advantage of having no overhead in the \gls{RTL} source code.
In GoAhead, these so-called interface connection primitives need to be declared and instantiated in the \gls{VHDL} source code.
Note that for both tools there is no (logic) overhead in the final design.
\par
For GoAhead, each interface located the (cardinal) border.
The number of signals must be specified, as well as the number of tiles (\glspl{CLB}) to use and reserve for the communication.
Both must relate and follow each other.
Increasing the number of signals also means adjusting the number of tiles to reserve for the connection macros.
Furthermore, it must match with the number of \glspl{LUT} and \gls{LUT}-pins available for each \gls{CLB}.
\par
% debugging
GoAhead is developed with Visual Studio adding extended debug capabilities to the application (e.g. breakpoints and intermediate views).
For \gls{TCL} based frameworks such as TedTCL and IMPRESS, those debugging capabilities are limited and in general, do not go without a code change (e.g. adding print and assert statements.).
On the other hand, GoAhead has no direct access to an open \gls{FPGA} design in Vivado.
It can only interact indirectly via script files.
This makes it impossible to make some specific design changes.
GoAhead requires the device files to be available for each device to retrieve the fabric information, whereas IMPRESS can just query this in Vivado.
\par
%Test
For comparison, example1 (from \Cref{sec:ex1}) is used.
This example has one module and one partition. \gls{RTL} source code is identical, the static system
The partition has a size of $4\times50$ tiles, the shape is a rectangle placed from tile X34Y99 to X38Y50.
The designs were built consecutively on the same desktop computer.
GoAhead took about 31 minutes and 47 seconds to complete the build\footnote{This is without generating the partial bitstream.}.
The IMPRESS framework completed in 8 minutes and 52 seconds.
The difference in time is mainly due to the number of nodes that are blocked.
The static blocker has 32750 nodes and the module blocker has 175040 nodes blocked.
14301 nodes are blocked by IMPRESS, only single fence nodes.
%For a $4\times50$ tiles partition this results in a script that contains 188038 wires to be blocked.
\par
GoAhead blocks all wires around the reconfigurable partition.
A GND net is created and all nodes to be blocked are assigned to that net (\Cref{lst:blocker-goahead}).
This ensures that the vendor router algorithm does not use these nodes (or wires).
\par
The IMPRESS framework uses a similar approach for blocking nodes.
There a blocking net is constructed by a set of nodes which they call a fence.
The same fence net is used for the static design and for the reconfigurable module.
Nodes are assigned to the net by using the \texttt{FIXED\_ROUTE} property to prohibit the router to use these nodes (\Cref{lst:blocker-impress}).
They are terminated by using the input buffer (IBUF) and output buffer (OBUF) primitives (see \Cref{fig:IMPRESS-blocker-net-schematic}).
\par
The blocking algorithm from IMPRESS is faster than the one used in GoAhead as we have seen above.
GoAhead blocks a lot more wires and this takes considerable more time to assign the blocking net and route the design.
The fence created by GoAhead is thicker.
Especially for the module, a whole lot more wires are blocked.
At this point, it is not known if this is done by design or that it was required at some point. It could also be the case that the algorithm is less optimal in certain situations.
\par
The partial bitstreams are generated after a successful build of the module by the IMPRESS framework.
GoAhead relies on \textsc{BitMan} tool to do this. The designer has to command the \textsc{BitMan} tool to slice out the module bitstream files for each module.

\begin{figure}[h!]
  \centering
  % Used the export to PDF option in Vivado
  \fbox{\includegraphics[width=0.5\textwidth, trim = 1cm 12cm 1cm 12cm]{im/relatedwork/IMPRESS-blocker-net-schematic.pdf}}
  \caption{IMPRESS blocker net in the RTL schematic.}
  \label{fig:IMPRESS-blocker-net-schematic}
\end{figure}

\begin{listing}[h!]
  \tclcode{code/relatedwork/blocker-impress.tcl}
  \caption{Blocker script of IMPRESS (partly listed).}
  \label{lst:blocker-impress}
\end{listing}

\begin{listing}[h!]
  \tclcode{code/relatedwork/blocker-goahead.tcl}
  \caption{Blocker script of GoAhead (partly listed).}
  \label{lst:blocker-goahead}
\end{listing}

\section{Module Stitching and Rapid Overlay}
For this work, there was a need to modify the routing on a low-level (netlist) basis of the design (as we will see later in \Cref{chap:implementation}).
Tools that are capable of interaction with the netlist such as RapidSmith \cite{RAPIDSMITH2} and RapidWright \cite{RAPIDWRIGHT} have been reviewed and considered for this work.
Those tools allow the designer to build customized \gls{FPGA} implementations and operate on a netlist level of the design.
However, installation of additional software is required, whereas Vivado already has most of all the necessary functionality.
\par
In \cite{rapid-overlay-builder}, a method is presented for module stitching without invoking the router.
This so-called stitching operation is also called \emph{zipping}, connects adjacent modules directly, without logic overhead.
The long place-and-route process is a growing concern and the vendor tools try to accelerate the compilation using different techniques.
Overlays are a way to solve reconfigurable computing problems.
Those overlay architectures act as pre-compiled circuits.
They propose a tool called ROB (Rapid Overlay Builder) that effectively builds those overlays from any logic design.
This tool addresses the issue of long place and route times that overlay architecture can have.
It can place and route any logic design into a set of adjacent modules.
