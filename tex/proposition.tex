\chapter{Proposition}
\label{chap:proposition}
As we have seen in \Cref{chap:relatedwork}, the frameworks that implement their own \gls{PR} flow can have limited test capabilities.
\Cref{tab:comparison-of-dpr-tools} shows that timing analysis is not supported by GoAhead.
The IMPRESS framework has better test and verification support than GoAhead.
The automated design flow checks the system configuration for potential mistakes.
Interface alignment issues can not occur, because, during implementation, the border nodes from the static system are used again for the modules.
Furthermore, it has support design reconstruction, capable of building a fully placed and routed design.
However, this design reconstruction is only supported for island reconfiguration style, fine-grained reconfiguration style is not supported.
Currently, GoAhead only generates the constraints files, but can not verify the configuration or to perform timing analysis.
Therefore, the main contribution of this work is to extend the GoAhead tool flow with design reconstruction capabilities.
This enables timing analysis for designs generated with the GoAhead tool flow.
It will have support for more fine-grained architectures, such as the grid-style variant.
%about grid-style, why is it important to have this?
%Having support for grid-style is advantageous in terms of storage and reconfiguration time.
%For x module variants x amount of storage memory is required for the partial bitstream.
%More variants is better, since this introduces more randomness in the power profile
% The reconfiguration time is directly proportional to the size of the bitstream file. The smaller the bitstreaam the lesser time it takes to reconfigure it
\par
The in this work proposed method (or tool) allows reconstructing the design for a given module configuration.
It is a contribution for designs generated for the GoAhead tool flow, by adding configuration validation and timing analysis.
This reconstruction overcomes some of the limitations on the testability of slot- and grid-style reconfigurable architectures as mentioned before.
The size of the reconfigurable region can be of size $M \times N$, where $M$ and $N$ are of arbitrary size.
To build a grid-style design, the GoAhead tool is used to generate the design constraints for the reconfigurable \gls{FPGA} system.
\autoref{fig:pr-overview} shows a general overview of the design flow together with the GoAhead tool flow.

\begin{figure}[h!]
  \centering
  \includegraphics[width=\textwidth]{im/overview.pdf}
  \caption{Overview of the proposed method.}
  \label{fig:pr-overview}
\end{figure}

\par
The static system is modified to create a fully placed and routed netlist by combining and merging the logic and routing configuration of multiple designs into one.
During this reconstruction, we can verify the correctness of a given set of modules and detect interface connection errors.
With the fully placed and routed netlist we can apply timing analysis and timing simulation.
\par
The full design must be identical to that of the run-time design, so some important physical constraints must remain intact.
The logic must be restored in the exact same place on the fabric.
Routing constraints must be kept, i.e. the signals must use identical wires on the fabric.
Route and placement constraints are device-dependent.
Therefore we operate on the post-implemented design which is the gate-level netlist.
\par
The tool will aid the user to find mistakes that would otherwise leave the design in a non-functional state.
It will be able to detect interface connection errors.
GoAhead will generate the design constraints for the reconfigurable system.
After completion of the implementation phase in Vivado, a merge script can be executed.
The script uses the design checkpoints and reconstructs the design with the following properties:
\begin{itemize}
  \setlength\itemsep{-0.5em}
  \item The user must be able to provide the interface mapping, module configurations.
  \item All configurations with all variants can be restored.
  \item Checks if valid interface mapping, e.g. signal width matches.
  \item For each module, if the signals match up with the dedicated wires, pin layout should be equivalent.
  \item The design should be reconstructed in such a way that it is functional and performance-wise identical as the design in run-time.
  \item Automatic timing analysis for each design.
\end{itemize}

\gls{TCL} scripting and Vivado was chosen mainly because:

\begin{itemize}
  \setlength\itemsep{-0.5em}
  \item Direct interaction with an open design.
  \item Has the knowledge of the fabric of the used device.
  \item Able to place and route the design manually and visually inspect the result.
  \item Report the timing properties.
  \item Equipped with a simulator.
\end{itemize}

\section{Merging Modules and Variants}
\label{sec:prop-merging-modules-variants}
A variant of a module has the same behavior, but different hardware implementation. As each variant has a different hardware layout, the timing (and other properties) may vary.
To verify the complete design, all possible (useful) module configurations have to be tested.
%Determining the number of module configurations is important.
%For example, if we have a set of $M$ modules and each module has $V$ variants then the total amount of configurations could be $V^M$.
%The maximum number configurations to test is likely to be less than stated above since not all configurations make a useful design.
Furthermore, modules may have other placements constraints.
The footprint of a module is such a constraint.
The module might not fit into a given location based on the hardware requirements (e.g. tiles that contain \acrshortpl{DSP} or \acrshortpl{BRAM} blocks etc.).

\section{Features}
\label{sec:prop-features}
\Cref{fig:pr-ft-fabric-merge-desired} shows a correctly placed module.
To mention some of the properties that the design reconstruction tool can check.
For example, in \Cref{fig:pr-ft-merge-width} the number of inputs or outputs signals of the static design, does not match with the (reconfigurable) module interface.
We can detect possible alignment errors that may occur as shown in \Cref{fig:pr-ft-merge-align}, where a different wire is expected on the fabric level.
Another verification feature is the detection of open circuits, or to check that the correct interface is used or present (\Cref{fig:pr-ft-merge-border}).
In that figure, the input signal is expected at the west border of the module, but the signal is applied at the north border.
Furthermore, able to detect pin or wire swaps (\Cref{fig:pr-ft-merge-pinswap}).
This can happen if, for example, the synthesis tool tries to optimize the design, resulting in pin or wire swaps near or at the border.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.75\textwidth]{im/pr-ft-fabric-merge-desired.pdf}
  \caption{Example of a correctly placed module on the FPGA fabric.}
  \label{fig:pr-ft-fabric-merge-desired}
\end{figure}

\begin{figure}[h!]
\centering
  \begin{subfigure}[t]{0.24\textwidth}
    \centering
	\includegraphics[width=1.0\textwidth]{im/pr-ft-merge-width.pdf}
	\caption{}
	\label{fig:pr-ft-merge-width}
  \end{subfigure}
  \begin{subfigure}[t]{0.24\textwidth}
	\centering
	\includegraphics[width=1.0\textwidth]{im/pr-ft-merge-align.pdf}
	\caption{}
	\label{fig:pr-ft-merge-align}
  \end{subfigure}
  \begin{subfigure}[t]{0.24\textwidth}
	\centering
	\includegraphics[width=0.85\textwidth]{im/pr-ft-merge-border.pdf}
	\caption{}
	\label{fig:pr-ft-merge-border}
  \end{subfigure}
  \begin{subfigure}[t]{0.24\textwidth}
	\centering
	\includegraphics[width=0.95\textwidth]{im/pr-ft-merge-pinswap.pdf}
	\caption{}
	\label{fig:pr-ft-merge-pinswap}
  \end{subfigure}
    \caption{Problems that might occur when incompatible modules are placed:
	(\textbf{\subref{fig:pr-ft-merge-width}}) the number of provided interface signals mismatches,
	(\textbf{\subref{fig:pr-ft-merge-align}}) wrong wire alignment on the interface border,
	(\textbf{\subref{fig:pr-ft-merge-border}}) the module expects input from the West border, but the North border is provided and
	(\textbf{\subref{fig:pr-ft-merge-pinswap}}) the wrong pins plugged at the border.}
   \label{fig:fr-ft-1}
\end{figure}

Regarding the placement of the module itself, misplacement can be detected in advance.
To ensure that no overlap occurs and the module is placed in the correct location of the fabric.
For example in \Cref{fig:pr-ft-merge-overlap}.
After the placement of Module 2, the configuration of Module 1 is overwritten, leaving the design in an undefined state.
It could also happen that the module is placed in (a part of) the static region.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.85\textwidth]{im/pr-ft-merge-overlap.pdf}
	\caption{Overlap of modules after placement. The configuration of Module 1 is overwritten by Module 2.}
	\label{fig:pr-ft-merge-overlap}
\end{figure}

For \gls{PR}, the design is split into a static and one or more dynamic parts.
Although we are capable of testing reconfigurable modules individually, the decoupling makes it impossible to do a timing analysis on the whole system.
Furthermore, the placement constraints that have been put on the design can affect the timing of the system as well.
For example, the output signals are routed alongside the partial region, the length of the signal paths increases, affecting the timing properties of that signal.
After reconstruction, we end up with a fully placed and routed netlist.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.7\textwidth]{im/pr-part-timing-example.pdf}
	\caption{Longer signal paths, different timing properties.}
	\label{fig:pr-partitioning-timing-1}
\end{figure}

Timing analysis (or timing simulation) checks if the design can operate at the specified clock frequency.
In case of timing violations, changes have to be made to the design, such as choosing a different module configuration or lower the clock frequency.
Different module configurations may lead to different operating frequencies, therefore we could find a more optimal module placement concerning the clock frequency.
\par
In the final stage, we end up with a full bitstream\footnote{Can additionally be performed on bitstream level with \textsc{BitMan}\cite{BITMAN}} that can also be used as an initial configuration.
