\chapter{Implementation}
\label{chap:implementation}
This section describes the implementation for the \emph{Design Reconstruction Tool} (DRT) as proposed in the previous chapter.
The DRT takes care of merging modules into the static design.
It is intended for the designs created with the GoAhead tool and uses the \gls{TCL} scripting of Vivado.
The starting point is the \gls{DCP} files that are the output of the synthesis tool.
They contain a snapshot of the design which includes the netlist, constraints, and implementation results.
The \gls{DCP} files are generated in conjunction with the constraints files from the GoAhead tool.
In total we have:
\begin{itemize}
  \setlength\itemsep{-0.5em}
  \item \gls{TCL} library: \emph{drt.tcl} (namespace eval drt).
  \item Input \gls{DCP} files: modules and static design.
  \item A mapping and module configuration definition.
\end{itemize}

\section{Design Reconstruction}
\label{sec:impl:design-reconstruction}
Regarding the design reconstruction, there are some points of attention.
Starting with the nets, they can only make use of the already existing nodes.
Any other modifications must be prevented at all times.
All nets should be properly terminated and not left floating, but as we see later, this is not always possible.
\par
The connection logic outside the module partition needs to be removed.
This logic is not present during runtime and can cause issues in the implementation of the static part of the design.
Furthermore, we must check that no logic gets unplaced during the placement of the module logic.
Unplaced logic can be a sign of logic overlap in the design and therefore it might not function as intended.

%Routes that are unterminated are called antennas.
%They can occur since a module or static interface may be unused.
%Such interfaces need to be either removed or properly terminated or the proxy logic needs to be restored.
During run-time reconfiguration we can have antennas, but during design time this is not allowed.
The design will not pass the design rule checks \acrshort{DRC} (unless we lower the rule check).
\par
The following subsections describes the sequence of the reconstruction step-by-step:
\begin{itemize}
  \setlength\itemsep{-0.5em}
  \item Prepare the designs (\ref{subsec:impl-prepare-designs}).
  \item Validate the user input, check existence of input files and mapping script (\ref{subsec:impl-validate-user-input}).
  \item Preserve anchor logic, before placing modules (\ref{subsec:impl-preserver-anchor-logic}).
  \item Place the modules into the static design (\ref{subsec:impl-place-modules}).
  \item Reconnect all interface nets (\ref{subsec:reconnect-nets}).
  %\item Handle all p2s nets that are not connected (\ref{subsec:handle-p2s-nets-unconnected}).
  %\item Restore design constraints (\ref{subsec:restore-anchor-logic}).
  \item Restore clock logic (\ref{subsec:impl-clock-logic}).
  \item Restore anchor logic s2p (\ref{subsec:restore-anchor-logic}).
  \item Final route of the design and bitstream generation (\ref{subsec:impl-finalize-design}).
\end{itemize}

After performing these steps, we can do a timing analysis and run a simulation.
Since we use Vivado in non-project mode, we must enter the commands manually in the console.
The DRT includes a script to launch the simulator on non-project mode.
It takes a \gls{DCP} file and a simulation script to set up and run the simulation.
Those commands and steps are described in \Cref{sec:impl:timing-analysis-and-simulation}.

\subsection{Prepare the Designs}
\label{subsec:impl-prepare-designs}
During the implementation phase, the design is optimized.
One such optimization is flattening the hierarchy of the design.
This flatting will, for example, coalesce certain nets into a single net.
Those nets will get a different name other than used in the \acrshort{HDL} source.
Although it is not a strict requirement for the design reconstruction,
adding annotations to the interface signals and keeping the design hierarchy makes it easier to do the signal mapping\footnote{Note that this can lead to a less optimal design}.
Without the signal annotating, the implemented design has to be opened and searched for the interface nets by hand.
%Note that for the simulator the same signal names can be used.
\par
In Vivado there exist a number of attributes to constraint the \acrshort{HDL} design \cite{UG901}.
The Save (S) constraint is a mapping constraint. During mapping, the nets are absorbed into logic blocks and some elements are optimized away.
The \emph{S} attribute prevents such optimizations in order to preserve access to nets in the post-synthesis netlist.
The Keep (KEEP) constraint preserves signals in the netlist.
Both \emph{S} and \emph{KEEP} constraints are applied to a signal in the \gls{VHDL} source code, an example is shown in \Cref{lst:impl-ex-attribute}.
To disable any flatting we can use \emph{flatten\_hierarchy none} with the synthesis command (\Cref{lst:impl-ex-flatten-hier}).
% ref: https://www.xilinx.com/support/documentation/sw_manuals/xilinx14_7/xst_v6s6.pdf

\begin{listing}[h!]
  \vhdlcode{code/implementation/impl-ex-attribute.txt}
  \caption{Signal names to 'keep' by defining the attributes in VHDL source.}
  \label{lst:impl-ex-attribute}
\end{listing}

% synth_design -part xc7z020clg484-1 -top top -keep_equivalent_registers -flatten_hierarchy none
%\todo{About this flattening switch}

\begin{listing}[h!]
  \tclcode{code/implementation/impl-ex-flatten-hier.txt}
  \caption{To keep the (signal) hierarchy, add the \emph{flatten\_hierarchy} switch to the synthesize command.}
  \label{lst:impl-ex-flatten-hier}
\end{listing}

\subsection{Validate User Input}
\label{subsec:impl-validate-user-input}
The interface definition for each module configuration must be provided beforehand.
For certain designs (e.g. when there is a one-to-one relation), we could deduce this from the module implementation.
Regarding grid-style architecture, this deduction is not always possible since the same module can be placed at various locations and using a different interface.
For each configuration the user has to define:
\begin{itemize}
  \setlength\itemsep{-0.5em}
  \item The runtime module configuration.
  \item The interface mapping.
  \item The name of the clock net.
  \item Indicate the unused interfaces (or slots).
\end{itemize}

The module mapping configuration is defined in a \gls{TCL} script file (see \Cref{lst:impl-ex-mapping}).
It contains a list of modules to be placed, together with the interface mapping.
The mapping defines which input is mapped to which output.
It is possible to map each signal like $x[0]=y[0], x[1]=y[1], ..., x[n]=y[n]$ manually.
However, Vivado allows the use of the * wildcard in the index field.
If all the index numbers of the signals line up with their index, manual unrolling is not required.

\begin{listing}[h!]
  \tclcode{code/examples/example1/mapping.tcl}
  \caption{Example of the mapping script file. Here were define the list of modules, the interface definition, and the name of the clock net.
  The script is sourced during the execution of the DRT.}
  \label{lst:impl-ex-mapping}
\end{listing}

For designs generated with GoAhead, we can identify two hierarchical levels: the connection logic and the module logic (\Cref{fig:module-connection-logic-examples}).
The connection logic can be seen as the switch box for routing in- and output signals outside the partial module.
The module logic contains the functional implementation for the partial module.
The number of in- and output ports from the connection logic can differ from the module Logic.
In \Cref{fig:partial-module-hier-levels-2} the output of the module logic is connected to 3 outputs ports of the partial module.
The connection logic net name is used in the mapping script.

\begin{figure}[h!]
  \centering
  \begin{subfigure}[t]{0.45\textwidth}
    \centering
    \includegraphics[width=0.7\textwidth]{im/partial-module-hier-levels-1.pdf}
    \caption{}
    \label{fig:partial-module-hier-levels-1}
  \end{subfigure}
  \begin{subfigure}[t]{0.45\textwidth}
    \centering
    \includegraphics[width=0.7\textwidth]{im/partial-module-hier-levels-2.pdf}
    \caption{}
    \label{fig:partial-module-hier-levels-2}
  \end{subfigure}
  \caption{Partial module connection logic examples. (\textbf{a}) Connection logic with 1 input and 1 output connection. Connection logic with 1 input and 3 output connections. (\textbf{b}) Here the single output of the module logic is present on the 3 outputs of the partial module.}
  \label{fig:module-connection-logic-examples}
\end{figure}

\subsection{Preserve Routing and Anchor Logic}
\label{subsec:impl-preserver-anchor-logic}
Some of the anchor logic might be required to get design closure.
This is the case when, for example, not all the ports from the partial area of the static design are used.
The anchor logic is removed when turning the partial area into a black box.
The black box cell has all of its logic removed and signals are chopped off at the border of the partial area.
Those signals are now defined to be antennas and therefore will not pass the \acrshort{DRC}.
The partition nets are connected to anchor \glspl{LUT} in the partial area.
Those \glspl{LUT} are (leaf) cells and need to be restored as well.
For each \gls{LUT} cell, we have to preserve the routing, pins, and the configuration (equation) of the anchor \gls{LUT}.

Physical constraints have to be added to the implemented designs.
These are the lock and fix route constraints to prevent the vendor tool to make any changes or do optimizations.
In particular, the \emph{LOCK\_PINS} constraint needs to be applied to all \glspl{LUT}.
This property specifies the mapping between logical \gls{LUT} inputs and the physical \gls{LUT} inputs~\cite[p.~150]{UG903}.
An example is shown in \Cref{lst:impl-ex-constraints}.
After placement, we have seen that pin swaps at the \glspl{LUT} can occur (see \Cref{fig:impl-issue-pin-swap-LUT}), which, as observed, can not always be resolved later on.

\begin{listing}[h!]
  \tclcode{code/implementation/impl-ex-constraints.txt}
  \caption{Example on how to apply the design constraints.}
  \label{lst:impl-ex-constraints}
\end{listing}

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.6\textwidth]{im/implementation/impl-issue-pin-swap-LUT.png}
  \caption{Input pins A1 and A2 are swapped. This can happen if the net has a different end point in the route string than the physical pin assignment of the LUT itself.}
  \label{fig:impl-issue-pin-swap-LUT}
\end{figure}

Reason to believe that this is a kind of optimization that happens in the background, e.g. using the fastest A6 input of a 6LUT or the A5 input for a 5LUT~\cite[p.~260]{UG912}.
%The behavior could be by design, a programmed behavior during the placement. For example, the first available LUT input is taken.
During the implementation of the design, \gls{LUT} pins are mapped in order from highest to lowest by default.
The highest logical pin is mapped to the highest physical pin.
%ug912,p260, %ug903,p150
%For all \glspl{LUT} we apply the \texttt{LOCK\_PINS} constraint to prevent pin swaps on the input.
\par
Furthermore, the \emph{IS\_ROUTE\_FIXED} property is applied to each net.
This prevents any changes in routing that could otherwise affect the timing and behavior of the final design.

\subsection{Place the Modules}
\label{subsec:impl-place-modules}
We can add logic from another design into a cell of static design with the
\newline \texttt{read\_checkpoint -cell <the cell>} command.
This command reads a \gls{DCP} file containing the implemented design and populates the given cell with the netlist from the checkpoint.
The command on its own is not suitable for merging multiple modules in the partial area. It can only be used once on an empty blackbox.
Therefore, in conjunction, the \texttt{add\_cells\_to\_pblock <pblock> [<cells>...]} is used.
First, an empty cell instance is created with the \texttt{create\_cell} command.
Normally, this command expects the \texttt{-reference <arg>} to be a type from the library cell.
The Library Cell contains all the cells that are applicable for the current device of the design.
It was found that Vivado creates custom cells with the \texttt{-reference} argument that have a unique name (i.e. not found in the Library Cell).
This way, we can create an empty cell for each module that we have.
For each module, a \gls{Pblock} is created. The empty cell is added to this \gls{Pblock}.
After that, the content of the module cell is added to this cell by reading the checkpoint.
\par
When all modules have been placed, the design is inspected on the existence of any unplaced logic cells.
The behavior of Vivado is that cells placed on the fabric will be unplaced when new cells are placed on the same position.
Using the \texttt{get\_cells filter \{STATUS=="UNPLACED"\}} command, we are able to find such cells and report this to the user.
Note that if any unplaced cells are found, the tool can not continue and the user has to manually resolve this.

\subsection{Reconnect the Interface Nets}
\label{subsec:reconnect-nets}
After module placement, the interface nets from the modules are placed on top of the interface nets of the static system.
The nets have to be fused since this is not performed by the vendor tool by default.
This is also called \emph{module stitching}, where the nets are stitched together.
Communication can only happen when two interface signals share a common node at the border of the reconfigurable region.
In case when the grid-style reconfigurable architecture is used, module-to-module communication happens via the common node at the border of the modules.
Therefore we have to identify the node (or the physical wire) that crosses the interface border.
If no such node is present, the module will not function as intended.
\Cref{fig:impl-ex-route-overlap} show this overlap in routing.

%In general, this is true for all input of the module.
%For the outputs we can have a different situation.
%It could be the case that not all outputs of a module are used for a certain module configuration
%That for example, a module having 3 outputs, internally routed to have the identical output value, but only one output is used for a certain configuration.

\begin{figure}[h!]
  \centering
  \begin{subfigure}[t]{1.0\textwidth}
    \centering
    \includegraphics[width=0.7\textwidth]{im/implementation/impl-route-overlap.pdf}
    \caption{}
    \label{fig:impl-route-overlap}
  \end{subfigure}
  \begin{subfigure}[t]{1.0\textwidth}
     \centering
     \includegraphics[width=0.7\textwidth]{im/implementation/impl-route-overlap-reconnect.pdf}
     \caption{}
     \label{fig:impl-route-overlap-reconstruct}
  \end{subfigure}
  \caption{Example route that has a wire that overlaps in both nets.}
  \label{fig:impl-ex-route-overlap}
\end{figure}

To find the common border node, we have to inspect the routing of the interface nets.
The route can be acquired by querying the \texttt{ROUTE} property using the \texttt{get\_property ROUTE [get\_nets <net name>]} command for the net specified.
As we have seen in \Cref{sec:routing-resources}, the command returns a list of nodes that forms the route on the fabric.
By default, the route string is in a relative form. The node names are not proceeded by their tile name.
For routing a design, this is not a problem.
As long as there is a defined begin and endpoint and there is only one way to go from one node to another.
However, with this ambiguity, we can not find the common node border node in the two interface nets to be fused.
In essence, we want to have the index of the node in the route string.
By including the tile information in the route string we can find the index.
There exist several methods to obtain the tile information and translate the route in an absolute route string:
\begin{itemize}[leftmargin=*]
  \setlength\itemsep{-0.5em}
  \item Using GoAhead \cite{GOAHEAD}, we could extract this information from the included model of the fabric.
        This information must be exported during the generation of the constraints scripts. Either the complete absolute net or the index in both nets could be of use.
  \item Using RapidWright \cite{RAPIDWRIGHT} which could work in the same way as GoAhead.
        The included model of the fabric is comparable to that of GoAhead tool, thereby it is able to retrieve an absolute route string. This requires processing in an external tool.
  \item Via \texttt{find\_routing\_path} command from Vivado, we can find a routing path between two or more nodes.
        This methods works if the tile information is present for begin and end node and only reliable for a single node hop.
        Multiple hops can result in a different route.
  \item Using the cardinal information of the nodes we can resolve the tile information. As we have seen in \Cref{sec:routing-resources}, we deduce the next tile from the node name.
        For example in \Cref{lst:impl-ex-nodes}. From third node, INT\_L\_X20Y25/EE4BEG3, the displacement is four into to the east direction (EE4).
        Adding 4 to the x-coordinate, we end up at node INT\_L\_X24Y25/EE4BEG3.
        We need at least one adjacent node that has the tile information to resolve the next node in the list.
        %Note there exist nodes that have no cardinal direction, but they are in general not used at the border.
  \item Via \texttt{get\_nodes <net>} we get the list of nodes of the corresponding net.
        These nodes can be used in the route string.
        By default the list returned by the \texttt{get\_nodes} command include tile information.
        However, the list is not in the order (the direction) of the nodes in the route string (when using \texttt{get\_property ROUTE [get\_nets net]})\footnote{Observed with Vivado 2018.3, but this could be different in later versions.}.
        Making this option not useful to resolve all nodes.
        It is only suitable for the unique nodes, commonly found at the begin and end of the route string.
  \item Via the \texttt{get\_pips} command has been found to be the best method and is used currently.
        An example output of this command is shown in \Cref{lst:get-property-route-example}.
        The \gls{PIP} list has the same order as the route string. From the \gls{PIP} name, we can extract the required tile information.
\end{itemize}

\begin{listing}[h!]
  \tclcode{code/implementation/impl-ex-nodes.txt}
  \caption{Example assigning nodes to the ROUTE property of a net. Vivado permits intermixing nodes with and without tile information.}
  \label{lst:impl-ex-nodes}
\end{listing}

For each interface signal, we can now find the index of the common border node in both nets.
There should be at least one common node, but it is likely to have multiple nodes if the net is fairly straight.
In case we have multiple nodes, we choose the first node (in front of an open brace).
When we have module-to-module nets, the last common node in the lowest nested branch is chosen.
\par
The next step is to combine the route of two nets into one.
Nets to travel from static system to the partial area are called \emph{s2p} nets.
The s2p nets have an output driver in the static system and or more inputs in the reconfigurable region.
For those s2p nets, the tail of the static net and the head of the partial net needs to be removed.
After that, the nets can be combined and applied to the driving net.
In Vivado, it is not possible to create a net alias from \gls{TCL} command.
Therefore, we clear the route of the trailing net such that we do not get conflicts in overlapping nodes.
The same is done for signals that are leaving the partial area.
Nets that go from partial area to static system are called \emph{p2s} nets.
In case the grid-style architecture is used, we can also define module-to-module nets \emph{m2m}.
\par
For modules that do not make use of any logic, but only use the routing resources, a number of additional steps have to be carried out.
These wire-only modules arise if, for example, in the \acrshort{HDL} the data is shifted.
Certain shift operations such as division or the shift rows operation from the \acrshort{AES} algorithm as we see later in \Cref{sec:ex-casestudy-aes}, only requires wires on the fabric.
The routing resources of those nets need to be added to the source module first.
After that, the final net combination can be made with the destination net.

\subsection{Restore Clock Logic}
\label{subsec:impl-clock-logic}
Clock signals are required for the correct operation of the modules in the partial region.
To ensure that the clock signal is available for each module, it must be connected and distributed on the reconfigurable region.
Likewise, for the interface signals, these clock resources and networks must be allocated and properly terminated in the reconfigurable region.
GoAhead provides the \texttt{ConnectClockPins} command (\Cref{lst:impl-ex-clockpins-goa}).
The command is executed on all tiles that are selected in the block view of GoAhead.
Logic elements on the fabric that require a clock signal (i.e. have a clock input pin) are instantiated by the generated \gls{TCL} statements.
For the \glspl{CLB}, the output registers are instantiated as flipflops.
Each clock pin of the register is connected to the global clock net.
During the implementation phase, this script is executed.
All registers that are present in the slices of the \glspl{CLB} are instantiated as \acrshort{FDRE} cells.
The clock input (CK) of each \acrshort{FDRE} flipflop is connected to the clock tree that spans two columns of \glspl{CLB}.
In the middle of the column, the clock net is connected to the global clock network.
\Cref{fig:reconnect-clocks-restore-anchor-logic} shows how restoration looks on the fabric level.

\begin{listing}[h!]
  \plaintext{code/implementation/impl-ex-connect-clockpins-goa.txt}
  \caption{GoAhead command to connect the clock pins}
  \label{lst:impl-ex-clockpins-goa}
\end{listing}

\begin{listing}[h!]
  \tclcode{code/implementation/impl-ex-connect-clockpins.txt}
  \caption{Example script to connect the clock pins in the partial area of the static design. Here all flipflops of SLICE\_X51Y99 are instantiated and connected to the global clock net.}
  \label{lst:impl-ex-clockpins}
\end{listing}

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.8\textwidth]{im/implementation/reconnect-clocks-restore-anchor-logic.pdf}
  \caption{The device view of the reconnected clock pins of each flipflop in each slice and the restored anchor logic.}
  \label{fig:reconnect-clocks-restore-anchor-logic}
\end{figure}

The placed clock logic and routing from the GoAhead script causes a physical overlap of \glspl{BEL} on the fabric with that of the module logic.
% blackbox
The \texttt{update\_design -cell <cell name> -black\_box} command removes all cells in the cell specified.
It does however not remove the clock net generated from the \gls{TCL} command from GoAhead, because it does not belong to that hierarchy.
The parent property is not set of those cells and can not be altered the current state of the design.
To restore the clock nets and logic after the placement of a module, the unplaced FDRE cells need to be removed first from the design.
These unplaced cells can be found by querying all cells that have the property \texttt{STATUS==UNPLACED}.
From the mapping script, the user must specify the name of the module clock nets.
The clock nets and pins are then merged with the clock net of the static design.

% % change hierarchy
% Can we use the \emph{rename\_cell} command like this:
% \mint[breaklines, fontsize=\small]{TCL}{rename_cell -to inst_PartialArea/_SLICE_X50Y89_DFF [get_cells SLICE_X50Y89_DFF -hierarchical]}
% It does not seem to change the hierarchy of the cell. The \emph{inst\_PartialArea} is ignored.

% % BELs
% Logical cells are mapped to physical BELs. Is there a way to detect if a BEL is already occupied?
% Two options that we have:
% \begin{itemize}
%   \item prevent the overlap from happening:
%   \todo{Preventing BEL overlap is possible by TCL script, option 2}
%   \begin{enumerate}
%     \item delete all and restore only the required logic.
%     \item only delete the occupied BELs, reconnect the clock nets
%   \end{enumerate}
%     \item delete the overlapping BEL instances after placement. This can be achieved by performing the following steps:
%   \begin{enumerate}
%     \item remove the clock nets of the modules.
%     \item disconnect the pins
%     \item remove the cells GoAhead placed previously
%     \item connect all clock pins of the module to the clock net.
%   \end{enumerate}
% \end{itemize}

\subsection{Restore Anchor Logic}
\label{subsec:restore-anchor-logic}
For certain configurations of modules, not all communication interfaces are required.
The reconfigurable region is cleared before any logic is placed.
This cuts all communication signals (nets) at the border of the partition.
Those nets have floating nodes and are marked as \texttt{ANTENNAS}.
Antennas are nets that contain nodes not properly terminated with a sink pin \cite{Subps}.
Floating nodes can also occur in a branch of the specified net.
By running a \acrshort{DRC} check or running the \texttt{report\_route\_status} command, Vivado will list all the nets that have a problem including antennas.
Vivado is able to remove such antenna nets, but this removal leads to unwanted optimizations in the route.
Therefore we restore as much as possible of the original anchor logic to prevent any floating nets.
Depending on the design, if the interface is not used, we could just clear the entire route to the partition.

\subsection{Finalize the Design}
\label{subsec:impl-finalize-design}
The intermediate signals used in the \acrshort{HDL} source for the interface signals need to be removed since they are not required anymore.
Furthermore, if a module turns into a blackbox, it needs to be removed as well.
After that, a final \texttt{route\_design} command is issued.
If no errors occurred at this point, we know that the provided module configuration is valid.
We should be able to write a \gls{DCP} file and a final bitstream file.
On this checkpoint, we can perform a timing analysis and run a functional simulation.
This is described in \Cref{sec:impl:timing-analysis-and-simulation}.

\section{Timing Analysis and Simulation}
\label{sec:impl:timing-analysis-and-simulation}
After a successful reconstruction, we have obtained the \gls{DCP} file on which we can perform timing analysis.
By running the \texttt{report\_timing\_summary}, Vivado reports if there are any timing violations in the design.
A clock constraint is required to perform any timing analysis on paths in the design.
To create clock constraint of 100 MHz, we issue the following command:
\mint[breaklines, fontsize=\normalsize]{TCL}{create_clock -name clk -period 10 -waveform {2.5 5} [get_ports clk]}
\par
Furthermore, a script is provided with the DRT to invoke the simulator.
This script writes a \gls{SDF} file, which contains all the timing-related information for each cell.
After that, the top-level unit is translated into executable code and linked to the simulation kernel to create an executable simulation snapshot.
Next, the simulator loads a simulation snapshot to affect the interactive simulation environment.

% \section{Binary identical bitstreams}
% \label{sec:impl:binary-identical-bitstreams}
% It is to be expected that a placed module has and identical partial bitstream as the partial bitstream of a module.
% That is to say, they should be binary equal in the partial area where the module resides.
% To make an comparison, the \textsc{BitMan} tool can be used the extract a partial bitstream from the full bitstream of the static system.
% The bitstream can then be compared with the partial bitstream from the module.
% If the bitstream files are identical, we are sure the reconstructed module is equal to the module in run-time.

% However, if not, we have explain or figure out why that is, and determine if this is a problem.
% It could be that:
% \begin{itemize}
%   \item The routes of nets are not identical (e.g. at the border)
%   \item Some added design constraints (such as LOCK\_PINS) change the bitstream file, but might not change the behavior (unknown, to be determined).
%   \item Not all proxy logic was removed/restored.\\
%   It still contains unused configured cells (e.g. LUTs or FFs) used for the proxy logic. Those cells do not affect the run-time design give a differences in the bitstream file.
% \end{itemize}
