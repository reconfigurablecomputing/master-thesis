\chapter{Introduction}
\label{chap:intro}
\glspl{FPGA} are general-purpose chips with a large number of programmable cells that can be programmed to form any logic circuit.
Its functionality can be altered after manufacturing, hence the name field-programmable.
The reconfigurable nature of \glspl{FPGA} is useful for prototyping applications and beneficial for systems that are susceptible for changes or future updates.
The configuration of an \gls{FPGA} is generally specified using a hardware description language.
After that, the configuration is translated which represents an electronic circuit to be mapped onto the fabric of the \gls{FPGA}.

\par
\gls{PR} is a feature that allows modification of certain predefined regions of the fabric of the \gls{FPGA}.
Here the fabric is divided into a static region and one or more dynamic regions.
During runtime, the dynamic regions can be reconfigured while the remaining design continues to function without interruption.
Hardware resources can be virtualized in a time-shared fashion. Increasing the logic density of the chip and larger designs may be implemented on smaller chips.
\gls{DPR} extends the design flexibility even further by allowing hardware modules to be loaded during run-time on demand.

\par
The use of \gls{DPR} introduces several design and implementation challenges.
Depending on the tools used, \gls{PR} designs require additional design steps such as partitioning, floorplanning and constraints that have to be applied on the design.
There exist various commercial and academic frameworks or tools to assist the designer in this process.
While the major vendor tools do have support and implement the \gls{DPR} design flow, they do not come without limitations.
One such limitation is the lack of more advanced reconfiguration styles such as slot- and grid-style \cite{Intel-UG-20136,UG909}, where the partial region can host multiple reconfigurable modules at the same time.
Module relocation is another missing feature, allowing the same modules to be reused in more than one partial region.

\par
Certain tools from the academic community such as \cite{GOAHEAD,IMPRESS} overcome these limitations or extend the vendor tool flow.
Adding new features or provide an automated and stand-alone framework that implements the whole system
In general, the commercial tools have a dependent design flow for building \gls{PR} systems.
Where dependent means that the static system and reconfigurable modules are developed in a single project.
The integrity and compatibility between static design and partial modules are kept in this way.
Furthermore, it provides verification of the complete system by means of a timing analysis and (post-implementation) simulation.
Independent design flow found in some of the academic tools allows the static system and reconfigurable modules to be designed independently from each other.
This can save implementation time, but a drawback of this decoupling is that the design is difficult to test as a whole.
Although the reconfigurable modules could be tested and simulated on their correct behavior, for the static system this is not possible by default.
Moreover, with different modules and configurations, interface mismatches and timing-related bugs are likely to occur.
With only in-circuit testing, it can be hard to point out where the real error originates.


\section{Problem Description}
\label{sec-problem-description}
Verification of \gls{FPGA} systems can be of a challenge, in particular, that of designs using \gls{PR}.
The academic tools that implement a grid-style reconfiguration architecture do not have the ability to perform a timing simulation.
Reason for this limitation is that the design was split into static and dynamic parts.
A full placed and routed design (static including partial modules) does not exist, thus a valid timing analysis can not be performed.
For such tools, functional testing and verification can only be done in-circuit.
This method of testing requires that the target hardware is known and present and this might not always be possible or practical.
In-circuit test can only be done in a late stage of the design.
If bugs arise in this late stage, such as timing violations, additional time must be spent to resolve these issues.
Timing-related bugs can be hard to detect.
This is mainly because they occur on-chip and are dependent on the given clock and data signals.
In the case of \gls{PR}, the timing can also be module configuration dependent.
%Module placement can have other timing behavior.
Testing the whole design before deployment into the field would be of added value.
Even if a module is successfully placed on the fabric during reconfiguration, there is no guarantee that the system behaves and functions as expected.

Each time when a new module is developed, the system as a whole should be tested again.
As the number of variants of a module increases, also the number of configurations and the number of tests that have to be performed increases.
In-circuit testing done manually can be cumbersome, inaccurate, time-consuming and error-prone, especially when grid-style reconfiguration architecture is used.
Therefore, automating the verification of systems that uses partial reconfiguration will be of added value for the designers.

\par
In this work, an automated tool for verification and design reconstruction is proposed for \gls{PR} systems.
The verification part checks if a given set of modules is compatible with each other and the static system.
A fully placed and routed netlist is obtained by merging reconfigurable modules back into the static design.
Since place and route constraints must be kept, the merging is done on a netlist level.
The so-called design reconstruction takes care of this merging process.
The logic of the modules that belong to a specific configuration is placed into the partial region of the \gls{FPGA} fabric and interface nets are reconnected.
Finally, we end up with a fully placed and routed design on which a timing analysis and functional simulation can be performed.

\newpage
\section{Thesis Outline}
\label{sec-thesis-outline}
This report is organized in chapters and sections and has the following structure.
First, the necessary background information is provided in \Cref{chap:background}.
The main topics are \gls{FPGA} architecture, design and verification flow and \gls{DPR}.
The related work is described \Cref{chap:relatedwork} summarizes the work found in the academic literature.
In \Cref{chap:proposition} a method is presented to overcome some of the mentioned limitations in the previous chapters.
Next, the realization of the tool that implements the proposed functionalities is described in \Cref{chap:implementation}.
Some examples are presented in \Cref{chap:examples} which also provides a way to check the correctness of the design reconstruction.
Finally, the conclusion and some possible future research directions are discussed.
