%IMPRESS framework
% This section will give a overview of the \gls{IMPRESS} framework. First a brief overview is given on how to setup and implement a reconfigurable system.
% After that, a more in-depth technical description of the framework and its properties.
% The analysis is indented for comparison with other academic tools for partial reconfiguration.
IMPRESS is an open-source automated tool for implementing reconfigurable systems \cite{IMPRESS, IMPRESS_tool_fine_grain_reconfig_systems, impress-fullpaper} intended for the Xilinx Zynq SoC \glspl{FPGA}.
The framework extends the Vivado reconfiguration flow capabilities by including a library written in \gls{TCL} language.
It thereby overcomes some of the limitations of the Xilinx design flow, such as the inability of stacking multiple \glspl{RP} within a clock region \cite{IMPRESS}.
The design flow for the IMPRESS framework is shown in \Cref{fig:impress-toolflow}.
Different types of granularities are supported in the same reconfigurable system (\Cref{fig:impress-multi-grain}).
For \emph{Coarse-grain} reconfiguration style, multiple \glspl{RP} can exist in the design. Each \gls{RP} will hold only a single module.
Exchanging modules during run-time adapts the behavior of the system.
The \emph{Medium-grain} style has the property that multiple \glspl{RM} can exist in the same \gls{RP}. Similar to the grid-style variant from \Cref{fig:reconfiguration-styles}(c), this variant allows direct module-to-module communication.
Useful, for example, in systolic array networks.
The \emph{Fine-grain} type has the capability to reconfigure low-level components.
For example, by changing the equation of a \gls{LUT}, the logic behavior can be altered during the run-time of the design.

% \begin{figure}[h!]
%   \centering
%   \includegraphics[width=0.5\textwidth]{im/impress/impress-toolflow.pdf}
%   \caption{The design flow for the IMPRESS framework.}
%   \label{fig:impress-toolflow}
% \end{figure}

% \begin{figure}[h!]
%   \centering
%   \includegraphics[width=0.5\textwidth]{im/impress/multigrain.png}
%   \caption{IMPRESS and Multi-grain Reconfiguration (from \href{https://des-cei.github.io/tools/impress}{https://des-cei.github.io/tools/impress}).}
%   \label{fig:impress-multi-grain}
% \end{figure}

\begin{figure}[h!]
  \centering
  \begin{subfigure}[t]{0.49\textwidth}
    \centering
    \includegraphics[width=1.0\textwidth]{im/impress/impress-toolflow.pdf}
    \caption{}
    \label{fig:impress-toolflow}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{0.49\textwidth}
    \centering
    \includegraphics[width=1.0\textwidth]{im/impress/multigrain.png}
    \caption{}
    \label{fig:impress-multi-grain}
  \end{subfigure}
  \caption{
    (\textbf{a}) The design flow for the IMPRESS framework.
    (\textbf{b}) IMPRESS and Multi-grain Reconfiguration (from \href{https://des-cei.github.io/tools/impress}{https://des-cei.github.io/tools/impress}).
  }
  \label{fig:impress-framework}
\end{figure}

Ease of use has a strong focus for the IMPRESS framework as mentioned by the authors.
By providing a project-style architecture, it allows validation before any implementation is performed.
This can be a time-saver since errors can be found in an early design stage.
The number of manual (or intermediate) steps that the designer has to perform are kept minimal.
After defining the system specification, the tool automatically performs all the implementation steps and (partial) bitstream generation.
Three straightforward text files are used to define the system specification. Having a profound understanding of the \gls{FPGA} fabric and architecture is therefore not required.
The following specification files are setup during design time:
\begin{itemize}
  \item \emph{project\_info file} is the starting point for setting up the reconfigurable system. Divided into three sections:
  \begin{itemize}
    \item General settings, such as project name and \gls{FPGA} chip to be used.
    \item Source locations of the static system.
    \item Source locations of the reconfigurable modules and the partition group definition per module.
  \end{itemize}
  \item \emph{virtual\_architecture} defines the architecture. E.g. the number of partitions in the static design, the partition size, location, and reference to the interface file.
  \item \emph{interface} file defines the local and global net interfaces.
\end{itemize}

The listings in \Cref{app:appendix-a} \ref{lst:impress-project-info}, \ref{lst:impress-interface} and \ref{lst:impress-virtual-architecture} show an example configuration.
This configuration was used for further clarification, understanding, and reference.
The decoupling of the implementation into a static system and reconfigurable modules (also called the partition phase) is a manual task.
This is done on the source level of the project.
When the project files have been created and the design is partitioned, a call to the IMPRESS framework can be issued in the \gls{TCL} console from Vivado:

\begin{code}
  \tclcode{code/impressproject/implement_reconfigurable_design.txt}
  \captionof{listing}{Implement the example reconfigurable design}
  \label{lst:implement-reconfigurable-design}
\end{code}

The first command makes the IMPRESS library available in Vivado by evaluating all the source files specified in the file given by the argument.
The second implements the system previously defined in the project file.
During synthesis and implementation, the design checkpoints are stored in the \emph{project\_name} directory near the project specification input files.
The generated bitstream files are stored in a subfolder. These files include the full bitstream of the static system and, for each module, a \gls{PB} file.
Additionally, an info file and corresponding \gls{DCP} file can be reviewed to find potential issues that might have occurred during the implementation phase.
%runtime and testing
\par
Regarding run-time management, the generated \glspl{PB} are intended (and prepared) for the run-time application.
This application runs on one of the two ARM processor cores of the Zynq \gls{FPGA}, called the \gls{PS}.
The \gls{PS} has full read and write access to the \gls{FPGA} configuration memory through the \gls{PCAP} interface.
This enables modifications to the circuit structure and functionality during operation of the \gls{PL} part of the \gls{FPGA}.
\par
%The following steps are executed when the \gls{TCL} proc \emph{implement\_reconfigurable\_design {project\_info\_file} } is called (\Cref{lst:implement-reconfigurable-design})
The following steps are performed when issuing the commands from \Cref{lst:implement-reconfigurable-design}:
%\footnote{From the provided examples and reviewing the source code from \href{https://github.com/des-cei/impress}{https://github.com/des-cei/impress}}:
\begin{enumerate}
  \item Parsing the project and virtual architecture files.
  \item Setup of the Vivado project and create the output folder structure.
  \item Static system generation.
  \item Reconfigurable module generation.
\end{enumerate}

Steps 1 and 2 do not require much further clarification.
We continue with the system implementation flow: static system and reconfigurable module generation.
After parsing the project input files the static system is synthesized.
\begin{itemize}
  \item \emph{obtain\_interface\_info} saves the interface information of all the reconfigurable partition groups to a \gls{TCL} variable.
  \item \emph{obtain\_xilinx\_and\_custom\_pblocks\_format} takes a \gls{Pblock} and saves its properties (e.g the size) to two formats: the Xilinx format and the custom format for the MORA PBS extractor tool.
  \item \emph{obtain\_max\_DSP\_and\_RAM\_for\_reconfigurable\_partitions} determines the number of \gls{DSP} and \gls{RAM} components (number of tiles) for each reconfigurable partition.
\end{itemize}

\par
%Floorplanning revers to the defined virtual architecture, the distribution of logic resources into \glspl{RP} on the \gls{FPGA} fabric.
The \glspl{RP} are arranged by their compatible footprint as well as with their compatible interface.
IMPRESS makes use of all inputs of the \glspl{LUT}.
Observing the implementation of the modules with respect to interfacing.
The user can specify which border is an input or an output.
The number of usable tiles, that is the width, can be defined by a suffix after specifying the cardinal direction.
For example, defining \emph{SOUTH\_0:3} for the interface allows that four tiles are to be used on the southern border.
Additional standalone signal routing is possible. For example, a commonly used asynchronous reset signal brings the \gls{FPGA} system to its initial state.
In GoAhead, it is possible to configure the interface for all four borders separately.
For each border, the designer can adjust the width, the width in the number of bits the interface must have. Furthermore, the border can be bidirectional.
Meaning it has separate input and output bus\footnote{For example the \mintinline{VHDL}{STD_LOGIC_VECTOR()}}.
Note that we do not mean a bidirectional bus here (declared with \mintinline{VHDL}{INOUT} keyword in \gls{VHDL}).

\subsubsection{Design reconstruction}
The term \emph{design reconstruction} refers to the operation on the previously decoupled designs reconstructing into a fully implemented and single design.
This implemented design is identical to the run-time design when all partial bitstreams are loaded.
Reconstruction is performed on the netlist level of the design since the place and route constraint must be kept for all designs.
IMPRESS framework offers support for design reconstruction.
A method is included to merge a module into the static system at design-time.
For this to work, the reconfigurable regions must be the same for static system and module, therefore the design reconstruction is only available for coarse-grained reconfigurable systems.
The provided method (a \gls{TCL} proc) requires additional information, which is output during the build and implementation phase of the reconfigurable system.
This additional information is stored in two text files and includes:
\begin{itemize}
  \item For each module, the clock, reset and interface nets and its route string property.
  \item Placement constraints of certain cells (e.g. the LOCK\_PINS etc.).
  %These properties: LOCK\_PINS IS_BEL_FIXED
\end{itemize}

The command listed in \Cref{lst:impress-dr-example} merges the \emph{div} reconfigurable module inside the reconfigurable partition of the static design.
After completion, the result is a fully implemented design, which can be validated using the simulator or by running a timing analysis.

\begin{listing}[!htbp]
  \vhdlcode{code/relatedwork/impress-dr-example.tcl}
  \caption{Example command for design reconstruction with the IMPRESS framework.}
  \label{lst:impress-dr-example}
\end{listing}

%NOTE: The merging of a reconfigurable module can only be done if the reconfigurable region of the static system and the reconfigurable module are the same. Therefore, it cannot be used in medium-grain reconfigurable systems.
%This section describes the attempt to merge a reconfigurable module into the static system at design-time.
%The authors mention that this is possible, but not for the medium-grain reconfigurable system (grid-style system).
%\textbf{The proc insert\_reconfigurable\_module} This proc can be found in script design\_reconstruction.tcl. It has 2 parameters, the name of the pblock in the static system and the dcp file of the module to be merged. The following commands I have tried for merging modules:
%There is also a folder called design_reconstruction that can be used to merge reconfigurable module into the static system at design-time.
%To see how this work open the checkpoint
%<path to IMPRESS>/examples/coarse_grain/impress_build/DESIGN_RECONSTRUCTION/static_system.dcp
%and then introduce the following command:
%insert_reconfigurable_module pblock_reconf_part_0 <path to IMPRESS>/examples/coarse_grain/impress_build/DESIGN_RECONSTRUCTION/group1_add.dcp
%this will merge the add reconfigurable module inside the reconfigurable partition.
%NOTE: The merging of a reconfigurable module can only be done if the reconfigurable region of the static system and the reconfigurable module are the same. Therefore, it cannot be used in medium-grain reconfigurable systems.