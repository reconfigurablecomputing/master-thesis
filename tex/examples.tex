\chapter{Examples}
\label{chap:examples}
This chapter provides a few example implementations for design reconstruction.
The first example is a minimal working example.
The last example is a case study that uses the grid-style partial reconfiguration architecture.

\section{Example 1: Minimal Working Example}
\label{sec:ex1}
% Example 1 is a minimal working example.
% Intended as proof of concept by demonstrating that the proposed method reconstructs the design equal to that of the runtime design.

\subsection{Implementation and results}
\label{subsec:ex1-impl-result}
Division is one of the four basic arithmetic operations that is the hardest to implement in hardware.
Mainly because addition, subtraction and multiplication are well defined and give exact answers.
The result of a division between integers will generally be a rational number, which in many cases can not be represented in binary.
This example implements 16-bit signed division in \acrshort{VHDL} using the division operator (/).
Information on which division algorithm is implemented when using the division operator is not published by Xilinx.
Furthermore, depending on the operand size and the chip used, different (more optimal) algorithms could be chosen for the implementation.
\Cref{lst:example1-div} shows the \acrshort{HDL} source code for this example.

% old paper: Comparison of arithmetic functions with respect to boolean circuit depth
% https://dl.acm.org/doi/pdf/10.1145/800057.808714
% paper: An Iterative Division Algorithm for FPGAs
% Space Efficient Division on FPGAs
% https://crisp.massey.ac.nz/pdfs/2006_ENZCon_206.pdf
% A Hardware Algorithm for Integer Division
% https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.513.7135&rep=rep1&type=pdf
% http://comparch.doc.ic.ac.uk/publications/files/pat99acssc.pdf
% Fast Division Algorithm with a Small NLookupTable

\par
If at any point in time an error is detected in the configuration (e.g. the specified interface nets) or a common border is not found or a (potential) overlap is detected, the reconstruction stops, and the error is reported to the user.
After a successful run, the design reconstruction, we can visually inspect the result in the device view.
The nets outside the partial region appear to be identical (\Cref{fig:ex1-device-fabric-compare}).
We can also observe that the correct border nodes (wires) are chosen by the tool, highlighted in the device view in \Cref{fig:ex1-border-nodes}.
Looking closer at the logic inside the partial area (purple rectangle, PBlock), additional clock logic can be seen.
After removing the clock logic cells and routing, the \glspl{PB} shows up as binary identical (\Cref{fig:ex1-bitstream-diff}).
% Question could be asked: does this additional clock logic affect timing properties of design?
% This adds extra 'load' the clock network, but does this affect the timing?

\begin{figure}[h!]
  \centering
  \begin{subfigure}[t]{0.30\textwidth}
    \centering
    \includegraphics[width=1.0\textwidth]{im/examples/ex1/ex1-bordernodes-a.png}
    \caption{}
    \label{fig:ex1-bordernodes-a}
  \end{subfigure}
  \large{$\rightarrow$}
  \begin{subfigure}[t]{0.30\textwidth}
    \centering
    \includegraphics[width=1.0\textwidth]{im/examples/ex1/ex1-bordernodes-b.png}
    \caption{}
    \label{fig:ex1-bordernodes-b}
  \end{subfigure}
  \large{$\rightarrow$}
  \begin{subfigure}[t]{0.30\textwidth}
    \centering
    \includegraphics[width=1.0\textwidth]{im/examples/ex1/ex1-bordernodes-c.png}
    \caption{}
    \label{fig:ex1-bordernodes-c}
  \end{subfigure}
  \caption{Border nodes used for interfacing, from left to right zoomed in on the border.}
  \label{fig:ex1-border-nodes}
\end{figure}

\begin{figure}[h!]
  \centering
  \begin{subfigure}[t]{0.3\textwidth}
    \centering
    \includegraphics[width=1.0\textwidth]{im/examples/ex1/ex1-device-module1.pdf}
    \caption{}
    \label{fig:ex1-device-module1}
  \end{subfigure}
  \begin{subfigure}[t]{0.3\textwidth}
    \centering
    \includegraphics[width=1.0\textwidth]{im/examples/ex1/ex1-device-static-empty.pdf}
    \caption{}
    \label{fig:ex1-device-static-empty}
  \end{subfigure}
  \begin{subfigure}[t]{0.3\textwidth}
    \centering
    \includegraphics[width=1.0\textwidth]{im/examples/ex1/ex1-device-static-module1.pdf}
    \caption{}
    \label{fig:ex1-device-static-module}
  \end{subfigure}
  \caption{Visual inspection of the reconfigurable partition in the device view. (\textbf{a}) The module to be placed. (\textbf{b}) The empty slot of the static system. (\textbf{c}) The final implemented design.}
  \label{fig:ex1-device-fabric-compare}
\end{figure}

\begin{figure}[h!]
  \centering
  \begin{subfigure}[t]{0.49\textwidth}
    \centering
    \includegraphics[width=1.0\textwidth]{im/examples/ex1/ex1-module1-clk-net.pdf}
    \caption{Module 1}
    \label{fig:ex1-module-clk-net}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{0.49\textwidth}
    \centering
    \includegraphics[width=1.0\textwidth]{im/examples/ex1/ex1-static-module1-clk-net.pdf}
    \caption{Combined}
    \label{fig:ex1-ex1-static-module1-clk-net}
  \end{subfigure}
  \caption{Clock nets}
  \label{fig:ex1-clk-net}
\end{figure}

\par
After we have obtained a fully placed and routed design, we can check the design for timing-related issues.
The maximum clock frequency for the design can be calculated as follows:

\begin{equation}
  f_{max}=\frac{1}{|t_{ACP} - t_{WNS}|}
\end{equation}

Where we define:

\begin{tabular}{ll @{ $=$ } l}
  \hspace{1cm} & $t_{ACP}$ & The Actual Clock Period.\\
  \hspace{1cm} & $t_{WNS}$ & The Worst Negative Slack.\\
\end{tabular}

\par
The \acrshort{WNS} can be retrieved by running the command \emph{report\_timing\_summary} from Vivado.
The \acrshort{ACP} is set in the XDC file of the design.
For Example 1, using a 100 MHz clock, the slack is negative. This means the design can not run on the frequency specified.
We get that the maximum operation frequency is\footnote{We do not include any input or output delays here, only the paths involving the clock.}:

\begin{equation}
  f_{max}=\frac{1}{|10ns - (-44.589ns)|} \approx 18.3 MHz
\end{equation}

Since we run in a non-project mode, we have to do the timing analysis in a manual fashion by entering commands in the \gls{TCL} console of Vivado.
\Cref{lst:example1-get-slack} shows how to get the timing information of the design by means of a \gls{TCL} script.
The listing shows how to add the clock constraint (if not already present) with the \texttt{create\_clock} command and how to query the timing paths.
This gives a quick pass/fail test if the design can operate on the given clock frequency.
The output of the timing analysis for 100 MHz is shown in \Cref{lst:example1-report-timing-summary-100MHz}, where it shows that the timing constraints are not met.

\begin{listing}[h!]
  \tclcode{code/examples/example1/get-slack.tcl}
  \caption{Script to get the timing related information of the design. The output is shown below the command.}
  \label{lst:example1-get-slack}
\end{listing}

% for WNS see http://www-classes.usc.edu/engr/ee-s/254/ee254l_lab_manual/Timing/handout_files/ee254l_timing.pdf

\begin{listing}[h!]
  \plaintextfootnotesize{code/examples/example1/report-timing-summary-100MHz.txt}
  \caption{The output of timing summary report of example 1 for a 100 MHz clock (partially displayed).}
  \label{lst:example1-report-timing-summary-100MHz}
\end{listing}

Moreover, we can simulate the design with a graphical simulator for functional testing.
Vivado has supports for multiple simulators.
For this work, we use the integrated Vivado Simulator.
\par
The static system contains a state machine that applies the values to the inputs  \emph{a} and  \emph{b} of the module.
At each clock edge, input \emph{a} of the module remains at the maximum value of 32767.
Input \emph{b} toggles between arbitrary values 3, 333 and 1.
%The number 32767 is chosen as the dividend and the divisor is an arbitrary chosen number.
\Cref{fig:ex1-sim-waveform} shows the simulation waveform of example 1.
In the begin of the waveform, we have a input clock of 10 MHz and at the end, the clock speed is 100 MHz.
It clearly shows that on 100 MHz the output value is wrong.
% WARNING: "/home/jeroen/Xilinx/Vivado/2018.3/data/verilog/src/unisims/FDRE.v"
% Line 150: Timing violation in scope /top_lib_work/module1/inst_Module/o_reg[6]/TChk150_5814 at time 691844 ps $setuphold (posedge C,negedge D,(0:0:0),(0:0:0),notifier,in_clk_enable_p,in_clk_enable_p,C_dly,D_dly)

\begin{figure}[h!]
  \centering
  \includegraphics[width=1.0\textwidth]{im/examples/ex1/ex1-sim-waveform.pdf}
  \caption{The waveform from the Vivado simulator.}
  \label{fig:ex1-sim-waveform}
\end{figure}

\par
To verify that the design reconstruction is correct, we can compare the bitstream files of the static system and module.
A placed module must have an identical partial bitstream as the partial bitstream of a module.
They must be binary equivalent.
The \textsc{BitMan} tool is used to extract a partial bitstream from the (full) bitstream of the static system.
After that, the extracted bitstream is compared with the partial bitstream of the module.
If the bitstream files are identical, we are sure the reconstructed module is equal to the module in run-time.

\begin{figure}[h!]
  \centering
  \includegraphics[width=1.0\textwidth]{im/examples/ex1/ex1-bitstream-diff.png}
  \caption{Bitstream file comparison using the \texttt{md5sum} and \texttt{diff} command, they show up as identical.}
  \label{fig:ex1-bitstream-diff}
\end{figure}

\par
Another thing we can do is place the \gls{PB} into the static bitstream and compare the static system on equality.
According to \citeauthor{BITMAN}, the generated full bitstream from Vivado is different from \textsc{BitMan}.
The contents and FAR value should be the same, but there are some differences in the commands.
Furthermore, the timestamp and Vivado version is different.
Printing the \gls{CLB} configuration with \textsc{BitMan} with the \emph{-c} switch does not show any differences.
Unfortunately, we can not compare the static system with this method\footnote{Another method would be to read back the configuration from the \gls{FPGA}, but this probably leads to the same result}.
The method works for the partial bitstream since we slice out the same region from the full bitstream and executing the commands consecutive gives the same timestamp.

% \todo{Different bitstream from Vivado}
% From the author:
% \begin{quote}
%     For the first question: yes, the partial bitstream generated by Bitman is different from Vivado.
%     The content and FAR value should be similar, but there may be some different in the other commands.
%     For example, in 7-Series, Vivado partial bitstream will disable all IOs, which I didn't want to. So in that case, I changed a couple of commands in the partial bitstream generated by Bitman to keep such IOs alive during reconfiguration.
%     Another case is for Zynq UltraScale+. The partial bitstream generated by Vivado disabled the ICAP, which I didn't want either. So I changed the commands to keep ICAP alive.
%     Moreover, I didn't write out the BRAM content in Bitman-generated partial bitstreams to keep the size of bitstreams smaller and speed-up the reconfiguration as most of time the BRAM content is all 0s.
% \end{quote}

% Some notes about these differences that I have found so far:
% \begin{itemize}
%   \item The timestamp and Vivado version is different in the header.
%   \item There are few bytes different, somewhere in the middle of the bitstream files.
%   \item Printing the CLB configuration with BITMAN  with the \emph{-c} switch does not show any differences.
% \end{itemize}

% \todo{Can be place the PBS into the static bitstream and compare also the static system on equallity?}
% This can be done as well with BITMAN \cite{BITMAN}: Placing a module and generating a statically linked bitstream
% Does not seem to be equal
% Placing a module and generating a statically linked bitstream
% For example, we want to merge the PACMAN with the LsM resource into the slot 21. According to Figure
% 1, we have its bottom-left corner at (42, 0).
% Page 3 of 11In this task, we need to do 2 actions by merging PACMAN module from the pacman_LsM.bit into the
% static PAC_007_STC_4.bit ( -m option) and writing new configuration data into another full bitstream ( -F
% option). Combined with above coordinators, we have the below command.
% bitman.exe –m 42 0 43 49 pacman_LsM.bit PAC_007_STC_4.bit –F merged_pacman_LsM_42_0.bit

% \section{Example 2: Restore proxy logic}
% \label{sec:ex2}
% \todo{Example2}
% An example with empty slots or unused interface.
% Provide a MWE.

\section{Example 2: Case Study AES encryption}
\label{sec:ex-casestudy-aes}
For this example, the \gls{AES} encryption algorithm is implemented on an \gls{FPGA} system using the grid-style architecture.
The goal is to prevent side-channel attacks and can be achieved by shuffling modules during runtime.
This results in different power profiles, thereby making side-channel attacks more difficult.
However, the operating frequency of the design could not be determined, it had to be guessed by lowering the clock speed.

%ref
%Securing Cryptographic Circuits by Exploiting Implementation Diversity and Partial Reconfiguration on FPGAs
%https://ieeexplore.ieee.org/document/8714801

\subsection{Background on AES encryption}
\label{subsec:aes-impl-background}
%About AES
The \gls{AES} is a fast and secure block cipher widely used for the encryption of data.
It is a symmetric encryption algorithm where the same key is used to encrypt en decrypt the data.
\gls{AES} is known for its speed and security.
Speed from the fact that it requires less computational power compared to asymmetric encryption algorithm.
Secure, because of a sophistical block cipher algorithm where the data is encrypted on a block basis.
\gls{AES} uses a fixed block size of 128 bits and key size of 128, 192 and 256 bits.
It operates on a 4x4 column-major order matrix of bytes.
The key size determines the number of rounds required to move the data to be encrypted through the cipher algorithm.
A 128 bits key requires 10 rounds, 192 bits requires 12 rounds and 14 rounds when 256-bit key is being used.

%ref:
%https://privacycanada.net/aes-encryption-guide/
%https://spanning.com/blog/aes-encryption/
%https://en.wikipedia.org/wiki/Advanced_Encryption_Standard
% worked out example:
%https://kavaliro.com/wp-content/uploads/2014/03/AES.pdf

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.7\textwidth]{im/aes/aes-algorithm.pdf}
  \caption{The AES encryption algorithm when a 128-bit key is used.}
  \label{fig:AES-algorithm}
  % Inspiration: https://www.researchgate.net/publication/4271757_A_novel_parity_bit_scheme_for_SBox_in_AES_circuits
\end{figure}

\begin{figure}[h!]
  \centering
  \begin{subfigure}[t]{0.49\textwidth}
    \centering
    \includegraphics[width=1.0\textwidth]{im/AES-SubBytes.pdf}
    \caption{SubBytes}
    \label{fig:AES-SubBytes}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{0.49\textwidth}
    \centering
    \includegraphics[width=1.0\textwidth]{im/AES-ShiftRows.pdf}
    \caption{ShiftRows}
    \label{fig:AES-ShiftRows}
  \end{subfigure}
  \begin{subfigure}[t]{0.49\textwidth}
    \centering
    \includegraphics[width=1.0\textwidth]{im/AES-MixColumns.pdf}
    \caption{MixColumns}
    \label{fig:AES-MixColumns}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{0.49\textwidth}
    \centering
    \includegraphics[width=1.0\textwidth]{im/AES-AddRoundKey.pdf}
    \caption{AddRoundKey}
    \label{fig:AES-AddRoundKey}
  \end{subfigure}
  \caption{AES encryption operation steps for each round (figures from \cite{wiki-aes}).}
  \label{fig:AES-rounds}
\end{figure}

%How AES works / what it does
The \gls{AES} algorithm shown in \Cref{fig:AES-algorithm} is an iterative algorithm.
Firstly, a bitwise XOR operation is performed between the encryption key and the plain text input data.
This operation gives the initial round key (note that RoundKey(0) is the starting key).
After that, a repeated sequence of four operation steps are performed (\Cref{fig:AES-rounds}):
\begin{itemize}
  \item \textbf{SubBytes}\\
  %https://www.researchgate.net/figure/Sub-Bytes-step-1-Byte-Substitution-The-byte-substitution-step-consists-of-replacing_fig6_322518289
  This byte substitution operation replaces each of the 16 bytes in the state matrix (input) with a fixed byte from a lookup table called the S-box.
  The S-box effectively maps an 8-bit input, to an 8-bit output. This ensures that the cipher has a non-linear component.

  \item \textbf{ShiftRows}\\
  The ShiftRows shifts the bytes in the static matrix by a certain offset. The first row is not changed. For the second row, each byte is shifted one to the left.
  For the third row, each byte is shifted by two to the left and three to the left for the fourth row.
  %https://en.wikipedia.org/wiki/Advanced_Encryption_Standard#The_SubBytes_step

  \item \textbf{MixColumns}\\
  In the Mix Columns step, each column of the state matrix is multiplied by a fixed polynomial represented by the matrix in \Cref{eq:lp-equation}.

  \item \textbf{AddRoundKey}\\
  In the final step for each round, the current state matrix is XORed with the RoundKey and is the input of the next iteration.
  It is identical to the very first operation.
\end{itemize}

\begin{equation}
  \label{eq:lp-equation}
  p =
  \begin{bmatrix}
          2 & 3  &  1  & 1\\
          1 & 2  &  3  & 1\\
          1 & 1  &  2  & 3\\
          3 & 1  &  1  & 2\\
  \end{bmatrix}
\end{equation}

For a 128-bit key, the above steps are repeated 9 times.
The last round does not have the MixColumns operation.
After finishing all rounds, the 128-bit input data is encrypted.
For decryption, the sequence from \Cref{fig:AES-algorithm} is inverted.

%Attacks on AES: Timing and Power
\par
Searching for weaknesses in \gls{AES} algorithm, there exist some types of attacks aimed at the physical implementation.
These types of attacks are, for example the timing and power side-channel attacks.
\citeauthor{Koeune99atiming} describes a timing attack method. Certain logic operations take more time to execute and differs on the input.
Measuring the time for each operation, an attacker can recover the key by working backwards.
%Paper: A timing attack against Rijndael
Another attack would be looking at the power consumption of \gls{AES} \cite{power-analysis-attacks-aes} and see \Cref{fig:aes-side-channel-attack}.
Differential power analysis is a common power analysis attack that is very effective against implementations of block ciphers like \gls{AES}.
The basic idea is to analyze the power consumption of the device for different cipher texts during encryption or decryption.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.5\textwidth]{im/aes/aes-side-channel-attack.pdf}
  \caption{Power analysis based side-channel attack on AES-128.}
  \label{fig:aes-side-channel-attack}
\end{figure}

%Counter measures
\par
There exist some countermeasures to overcome some physical attacks on the \gls{AES} implementation.
One method could be to switch the key frequently enough such that the attacker can not learn enough about the key in time.
Another way would be to make the power consumption independent from the processed input values.
This can be achieved by making the power consumption more random (or equal) for each clock cycle.
An example for achieving power side-channel protection is by using partial reconfiguration \cite{achieve-sca-dpr, sec-crypt-pr}.
\par
Leakages can often be found on power traces during transitions of the combinatorial circuit after a change of a driving register.
A common target on side-channel attacks on symmetric block ciphers is the output of the non-linear substitution layer of the algorithm.
\par
In \cite{achieve-sca-dpr} the authors use the idea of random S-Box decomposition realized by configurable lookup tables.
The standard S-Box is split into two random mappings and the correct S-box output is never stored onto a register.
Using \gls{DPR}, the dynamic decomposition was applied for the S-Boxes, including pre-charging of registers\footnote{Consecutive round inputs stored into registers can cause leakage.
By expanding the single register into two registers the leakage is avoided.}.
After running the power traces, they were not able to detect any first-order\footnote{When only one probe is used on the design.} side-channel leakage.
\par
The following subsection describes another countermeasure to prevent power side-channel attacks.
% First-order: only one probe on the design?
% Utilized Register Pre-charge: Consecutive round inputs are stored into registers can cause leakage.
% By expanding the single register into two registers the leakage is avoided

\subsection{Implementation and Results}
\label{subsec:aes-impl-result}
The starting point for this example is the implementation and continuation of the case study from \citetitle{Tom} \cite{Tom}.
The idea is to use random module configurations and module implementation variants, resulting into a more random power profile to make side-channel attacks more difficult.
To have multiple modules in the same partial region, the grid-style reconfiguration architecture was used.
Along with the use of \gls{DPR}, the physical implementation of the encryption algorithm can be altered during runtime.
The design is implemented on the ZedBoard, a development board for the Xilinx Zynq 7000 \acrshort{SoC}.
Three operations of the \gls{AES} encryption algorithm were relocated to the partial region: the SubBytes, ShiftRows and the MixColumns.
The static system controls the sequence and the number of rounds of the algorithm.
As mentioned before, timing analysis could not be performed on grid-style reconfiguration systems.
Using the proposed reconstruction tool, this can be done now.
Two configurations are tested.
Each configuration implements a variant of the SubBytes, ShiftRows and MixColumns operation, see \Cref{fig:aes-variants}.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.4\textwidth]{im/aes/AES-variants.pdf}
  \caption{Different AES configurations and module variants}
  \label{fig:aes-variants}
\end{figure}

\par
The static system acts as the testbench for the \gls{AES} implementation.
A \gls{TCL} script is executed during simulation. This script sets up the wave from signals and provides the stimulus to the inputs of the top entity.
Those inputs are mapped to real inputs of the \gls{FPGA} chip.
This simulates the switches and buttons on the Zedboard.
For the \gls{AES} algorithm, the last 4 bits of input data is applied via switches present on the ZedBoard.
The last 4 bits of the \gls{AES} are output on four LEDs of the ZedBoard.
These outputs can now also be observed in the simulator (\Cref{fig:ex-aes1-sim-waveform} and \Cref{fig:ex-aes2-sim-waveform}).
\Cref{fig:ex-aes1-timing-report} and \Cref{fig:ex-aes2-timing-report} shows that the timing constraints are met for this design.
Two input values are used for testing the designs\footnote{The output was checked using \href{http://aes.online-domain-tools.com}{http://aes.online-domain-tools.com}}.
For input value 6 we get:
\begin{itemize}
  \setlength\itemsep{-0.5em}
  \item input:  3243F6A8885A308D313198A2E0370736
  \item key:    2B7E151628AED2A6ABF7158809CF4F3C
  \item output: 481A0AAC6942EA7F426d3A5BD4B25fAD
\end{itemize}

And for input value 1 we get:

\begin{itemize}
  \setlength\itemsep{-0.5em}
  \item input:  3243F6A8885A308D313198A2E0370731
  \item key:    2B7E151628AED2A6ABF7158809CF4F3C
  \item output: F91914CD01924B124C2EC316B4B35A79
\end{itemize}


\begin{figure}[h!]
  \centering
  \begin{subfigure}[t]{1.0\textwidth}
    \centering
    \includegraphics[width=1.0\textwidth]{im/examples/aes/aes1-timing-report.pdf}
    \caption{}
    \label{fig:ex-aes1-timing-report}
  \end{subfigure}
  \begin{subfigure}[t]{1.0\textwidth}
    \centering
    \includegraphics[width=1.0\textwidth]{im/examples/aes/aes1-sim-waveform.pdf}
    \caption{}
    \label{fig:ex-aes1-sim-waveform}
  \end{subfigure}
  \caption{Test results of AES configuration 1, (\textbf{a}) shows the timing report and (\textbf{b}) the waveform from the Vivado simulator.}
  \label{fig:ex:aes1}
\end{figure}

\begin{figure}[h!]
  \centering
  \begin{subfigure}[t]{1.0\textwidth}
    \centering
    \includegraphics[width=1.0\textwidth]{im/examples/aes/aes2-timing-report.pdf}
    \caption{}
    \label{fig:ex-aes2-timing-report}
  \end{subfigure}
  \begin{subfigure}[t]{1.0\textwidth}
    \centering
    \includegraphics[width=1.0\textwidth]{im/examples/aes/aes2-sim-waveform.pdf}
    \caption{}
    \label{fig:ex-aes2-sim-waveform}
  \end{subfigure}
  \caption{Test results of AES configuration 1, (\textbf{a}) shows the timing report and (\textbf{b}) the waveform from the Vivado simulator.}
  \label{fig:ex:aes2}
\end{figure}

Both waveforms show the correct output on the LEDs.
For the maximum clock frequency we get for configuration 1:
\begin{equation}
  f_{max}=\frac{1}{|10ns - (26.468ns)|} \approx 31 MHz
\end{equation}

And for configuration 2 we get:
\begin{equation}
  f_{max}=\frac{1}{|10ns - (21.671ns)|} \approx 27 MHz
\end{equation}