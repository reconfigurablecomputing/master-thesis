% About GoAhead tool
The \textsc{GoAhead} tool is the successor of ReCoBus-builder tool \cite{GOAHEAD, goahead-build-pr-systems} supporting more features and newer devices.
This \gls{PR} tool targets the Xilinx \glspl{FPGA} for implementing run-time reconfigurable systems.
The tool is designed for usability by abstracting most of the low-level details from the design engineer.
Originally developed for the Xilinx ISE IDE, where it is able to interact through XDL with the Xilinx tools.
Support for XDL was dropped in Vivado, but GoAhead has been adapted to emit \gls{TCL} code instead.
GoAhead is a stand-alone .NET C\# application and provides a graphical user interface and a scripting interface.
This scripting capability allows automating the implementation process in a single shot batch job.
The tool provides floorplanning capabilities, communication interface generation, and constraints generation required for the implementation phase.

\begin{figure}[h!]
  \centering
  \begin{subfigure}[t]{0.49\textwidth}
    \centering
    \includegraphics[width=1.0\textwidth]{im/goa-gui-info.png}
    \caption{}
    \label{fig:goahead-gui}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{0.49\textwidth}
    \centering
    \includegraphics[width=1.0\textwidth]{im/goahead-toolflow.png}
    \caption{}
    \label{fig:goahead-toolflow}
  \end{subfigure}
  \caption{
    (\textbf{a}) The GUI of GoAhead.
    (\textbf{b}) The design flow for building reconfigurable systems with the GoAhead tool.
    The design of the static system and modules is completely separated.
    Floorplanning is done via de GUI of GoAhead. The corresponding constraints and VHDL templates are generated by the tool.
    After that, Vivado applies these files during the implementation phase of the design. Figure from \cite{Tom}.
  }
  \label{fig:goahead-framework}
\end{figure}

\par
The tool flow of GoAhead is depicted in \Cref{fig:goahead-toolflow}.
In the planning phase, the interface specifications are defined and resource budgeting is performed to calculate the minimal size of the partial area.
The designer has to take care of partitioning the design into a static and dynamic part.
In \Cref{fig:goahead-gui} the block view of the \gls{FPGA} device is shown where each tile resource type has a distinct color.
The block view is stored into device descriptions files (\emph{*.binFPGA}), a custom GoAhead format that comes along with the tool.
After the planning phase, GoAhead is used to floorplan the design (\Cref{fig:goahead-toolflow}).
The definition of the reconfigurable areas can be selected manually or by using the custom script commands of the tool.
From the selection, GoAhead generates the design templates (\gls{RTL} sources) and constraint files.
The \gls{RTL} sources have to be added to the Vivado project. The constraints files are executed at different stages of the design.
Using goa script files is a preferred method for building reconfigurable systems with GoAhead.
\par
To create a project with GoAhead (Vivado) using scripts, the following has to be done.

\begin{itemize}
  \setlength\itemsep{-0.5em}
  \item Define the static.goa file, which holds the commands:
  \begin{itemize}
    \setlength\itemsep{-0.5em}
    \item Floorplan the partial area
    \item Generate interface constraints
    \item Generate connection primitives in VHDL format
    \item Generate placement constraints
    \item Generate blocker macro
  \end{itemize}
  \item module.goa file
  \begin{itemize}
    \setlength\itemsep{-0.5em}
    \item Floorplan the module area
    \item Generate interface constraints
    \item Generate connection primitives in VHDL format
    \item Generate blocker macro
    \item Generate placement constraints
  \end{itemize}
  \item build.tcl script file for the static design and for each module.
\end{itemize}

Running the goa scripts generates all the \gls{RTL} sources and constrains scripts.
The \gls{RTL} sources must be included in the \gls{FPGA} project.
For the constraints scripts, they are executed in specific order after synthesis.
