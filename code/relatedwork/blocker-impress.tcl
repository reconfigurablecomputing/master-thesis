# IMPRESS:
# <skipped some code >
# Add the FIXED_ROUTE constraint
set froute [list]
  foreach node $nodes {
    lappend froute GAP $node
  }
set_property FIXED_ROUTE [list $froute] $net

# Final route string:
get_property FIXED_ROUTE [get_nets external_fence]
 { IOB_IBUF1 GAP INT_L_X38Y54/EE4BEG3 GAP INT_L_X38Y64/NE6BEG0 GAP INT_L_X38Y64/NE6BEG1 GAP INT_L_X38Y64/NE6BEG2 GAP ... }
