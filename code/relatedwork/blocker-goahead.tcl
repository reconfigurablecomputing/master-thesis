# GoAhead
create_net blocker_net_BlockSelection
create_pin -direction OUT gnd_for_BlockSelection/G
connect_net -net blocker_net_BlockSelection -objects [get_pins gnd_for_BlockSelection/G]
set_property ROUTE "( \ { INT_L_X34Y99/LVB_L12 INT_L_X34Y99/WW4BEG2 } \ { ... } ... )" [get_nets blocker_net_BlockSelection]

