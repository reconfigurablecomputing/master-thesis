#!/usr/bin/tclsh

# Variable declaration
set e 2.7182
puts $e;       # prints 2.7182
puts [set e];  # prints 2.7182

# List example
set alist {4 8 15 16 23}
set blist [list 4 8 15 16 23]; # another list initialization method
lappend alist 42
puts [lindex $alist 0]; # list index are zero-based, prints 4
puts [llength $alist];  # prints 6

# Dictionary example
set d [dict create]
dict append d key1 val1
dict append d key2 val2
puts [dict get $d key1]; # prints 'val1'

# Create a namespace
namespace eval example {
  namespace export example_proc
  variable x 1

  proc example_proc {} {
     variable x
     incr x
     puts $x
  }
}

# Calls the example_proc in the example namespace
example::example_proc; # prints 2