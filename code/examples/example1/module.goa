# Description: Module - 1 Location - 1
#-----------------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------------
# Global variables
#-----------------------------------------------------------------------------------------
Set Variable=ProjectDir Value=M:\2018.3\zedboard\thesis\examples\example1\goahead_pr;
Set Variable=MODULE_NAME Value=module1;
Set Variable=ZedBoard Value=%GOAHEAD_HOME%/Devices/xc7z020clg484.binFPGA;
Set Variable=GoAheadSources Value=%ProjectDir%/%MODULE_NAME%/goahead/sources;
Set Variable=GoAheadTcl Value=%ProjectDir%/%MODULE_NAME%/goahead/tcl;

#-----------------------------------------------------------------------------------------
# Load device
#-----------------------------------------------------------------------------------------
OpenBinFPGA FileName=%ZedBoard%;

#-----------------------------------------------------------------------------------------
# Floorplan the module area
#-----------------------------------------------------------------------------------------
# 5 columns module
ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X34Y99 LowerRightTile=INT_L_X38Y50;
ExpandSelection;
StoreCurrentSelectionAs UserSelectionType=ModuleArea;

#-----------------------------------------------------------------------------------------
# Generate interface constraints
#-----------------------------------------------------------------------------------------
# Slot (0,0) Border - West - Input
ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X34Y90 LowerRightTile=INT_L_X34Y83;
StoreCurrentSelectionAs UserSelectionType=X0Y0_WestBorderInput;
PrintInterfaceConstraintsForSelection
	FileName=%GoAheadTcl%/module-interface-constraints.tcl
	Append=False
	CreateBackupFile=False
	SignalPrefix=x0y0
	InstanceName=inst_ConnectionPrimitiveWestInput
	Border=West
	NumberOfSignals=32
	PreventWiresFromBlocking=True
	InterfaceSpecs=In:2-4:s2p_w;

InstantiateConnectionPrimitives
	LibraryElementName=LUTConnectionPrimitive
	InstanceName=inst_x0y0_w_in
	NumberOfPrimitives=32;

AnnotateSignalNamesToConnectionPrimitives
	InstantiationFilter=inst_x0y0_w_in.*
	InputMappingKind=internal
	OutputMappingKind=external
	SignalPrefix=x0y0
	InputSignalName=1
	OutputSignalName=s2p_w
	LookupTableInputPort=3;

# Slot (0,0) Border - West - Output
ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X34Y90 LowerRightTile=INT_L_X34Y83;
StoreCurrentSelectionAs UserSelectionType=X0Y0_WestBorderOutput;
PrintInterfaceConstraintsForSelection
	FileName=%GoAheadTcl%/module-interface-constraints.tcl
	Append=True
	CreateBackupFile=False
	SignalPrefix=x0y0
	InstanceName=inst_ConnectionPrimitiveWestOutput
	Border=West
	NumberOfSignals=16
	PreventWiresFromBlocking=True
	InterfaceSpecs=Out:2-4:p2s_w;

InstantiateConnectionPrimitives
	LibraryElementName=LUTConnectionPrimitive
	InstanceName=inst_x0y0_w_out
	NumberOfPrimitives=16;

AnnotateSignalNamesToConnectionPrimitives
	InstantiationFilter=inst_x0y0_w_out.*
	InputMappingKind=external
	OutputMappingKind=internal
	SignalPrefix=x0y0
	InputSignalName=p2s_w
	OutputSignalName=dummy_w
	LookupTableInputPort=3;

#-----------------------------------------------------------------------------------------
# Generate connection primitives in VHDL format
#-----------------------------------------------------------------------------------------
# connection primitives that are connected to the input signals of the module
PrintVHDLWrapper
	InstantiationFilter=inst_x0y0_w_in.*
	EntityName=ConnectionPrimitiveWestInput
	FileName=%GoAheadSources%/ConnectionPrimitive.vhd
	Append=False
	CreateBackupFile=False;

# connection primitives that are connected to the output signals of the module
PrintVHDLWrapper
	InstantiationFilter=inst_x0y0_w_out.*
	EntityName=ConnectionPrimitiveWestOutput
	FileName=%GoAheadSources%/ConnectionPrimitive.vhd
	Append=True
	CreateBackupFile=False;

#-----------------------------------------------------------------------------------------
# Tunnels
#-----------------------------------------------------------------------------------------
ClearSelection;
AddBlockToSelection UpperLeftTile=INT_R_X29Y90 LowerRightTile=INT_R_X33Y83;
StoreCurrentSelectionAs UserSelectionType=IOWestTunnel;
DoNotBlockDoubleEast;
DoNotBlockQuadEast;
DoNotBlockDoubleWest;
DoNotBlockQuadWest;

#-----------------------------------------------------------------------------------------
# Generate blocker macro
#-----------------------------------------------------------------------------------------
ClearSelection;
AddBlockToSelection UpperLeftTile=INT_R_X29Y109 LowerRightTile=INT_L_X42Y40;
SelectUserSelection UserSelectionType=ModuleArea;
StoreCurrentSelectionAs UserSelectionType=BlockerArea;

BlockSelection
	PrintUnblockedPorts=False
	Prefix=blocker_net
	BlockWithEndPips=True
	NetlistContainerName=default_netlist_container;

#SaveAsBlocker
#	NetlistContainerNames=default_netlist_container
#	FileName=%GoAheadTcl%/module-blocker.tcl
#	CreateBackupFile=False;

SaveAsBlocker
	NetlistContainerNames=default_netlist_container
	FileName=%GoAheadTcl%/module-blocker.tcl;

#-----------------------------------------------------------------------------------------
# Generate placement constraints
#-----------------------------------------------------------------------------------------
# module area
ClearSelection;
SelectUserSelection UserSelectionType=ModuleArea;

PrintAreaConstraint
	InstanceName=inst_Module
	AddResources=True
	FileName=%GoAheadTcl%/module-placement-constraints.tcl
	Append=False
	CreateBackupFile=False;

PrintExcludePlacementProperty
	InstanceName=inst_Module
	FileName=%GoAheadTcl%/module-placement-constraints.tcl
	Append=True
	CreateBackupFile=False;

# connection primitives connected to the input interface of the module
ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X26Y99 LowerRightTile=INT_L_X26Y92;
ExpandSelection;
StoreCurrentSelectionAs UserSelectionType=ConnectionPrimitiveWestInput;

PrintAreaConstraint
	InstanceName=inst_ConnectionPrimitiveWestInput
	AddResources=True
	FileName=%GoAheadTcl%/module-placement-constraints.tcl
	Append=True
	CreateBackupFile=False;

# connection primitives connected to the output interface of the module
ClearSelection;
AddBlockToSelection UpperLeftTile=INT_L_X26Y98 LowerRightTile=INT_L_X26Y91;
ExpandSelection;
StoreCurrentSelectionAs UserSelectionType=ConnectionPrimitiveWestOutput;

PrintAreaConstraint
	InstanceName=inst_ConnectionPrimitiveWestOutput
	AddResources=True
	FileName=%GoAheadTcl%/module-placement-constraints.tcl
	Append=True
	CreateBackupFile=False;
