# Create a clock object
create_clock -name clk -period 10 -waveform {2.5 5} [get_ports clk]

# Get the period of the clock (ACP, in ns)
set period [get_property PERIOD [get_clocks clk]]
# 10.000

# Get the timing slack (WNS)
set slack [get_property SLACK [get_timing_paths]]
# -44.589

# The max. clock frequency (in Hz):
set max_clock [expr { 1 / ($period - $slack) * 1000000000}]
# 18318708.897396915

report_timing_summary